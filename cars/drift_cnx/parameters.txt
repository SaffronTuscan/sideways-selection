{

;============================================================
;============================================================
; Honma CNX
;============================================================
;============================================================
Name      	"Honma CNX"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\drift_cnx\body.prm"
MODEL 	1 	"cars\drift_cnx\wheelleft.prm"
MODEL 	2 	"cars\drift_cnx\wheelright.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\drift_cnx\car.bmp"
COLL 	"cars\drift_cnx\hull.hul"
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
;)Statistics            TRUE
Class      	1 			
Obtain     	0 			
Rating     	-200			
TopEnd     	3167.066406 			
Acc         	5.079963 			
Weight     	1.100000 			
Handling   	50.000000 			
Trans      	1 			
MaxRevs    	0.500000 			

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			
SteerMod   	0.400000 			
EngineRate 	4.500000 			
TopSpeed   	35.500000 			
DownForceMod	2.000000 			
CoM        	0.000000 -9.000000 1.000000 		
Weapon     	0.000000 -32.000000 64.000000 		

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			
Offset     	0, 0, 0 		
Mass       	1.100000
Inertia    	1050.000000 0.000000 0.000000
           	0.000000 1675.000000 0.000000
           	0.000000 0.000000 850.000000
Gravity		2200 			
Hardness   	0.000000
Resistance 	0.001000 			
AngRes     	0.001000 			
ResMod     	25.000000 			
Grip       	0.010000 			
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.000000 9.000000 34.250000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	20910.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	6.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.003000
StaticFriction  	1.060000
KineticFriction 	1.010000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	21.000000 9.000000 34.250000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.500000
EngineRatio 	20910.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	6.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.003000
StaticFriction  	1.060000
KineticFriction 	1.010000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.000000 9.000000 -32.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	FALSE
SteerRatio  	0.100000
EngineRatio 	20000.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	7.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.004000
StaticFriction  	0.940000
KineticFriction 	0.990000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	21.000000 9.000000 -32.500000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	FALSE
SteerRatio  	0.100000
EngineRatio 	20000.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	7.000000
SkidWidth   	8.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.004000
StaticFriction  	0.940000
KineticFriction 	0.990000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	12.900000
Restitution 	-0.840000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	12.900000
Restitution 	-0.840000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	12.900000
Restitution 	-0.840000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	700.000000
Damping     	12.900000
Restitution 	-0.840000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	-1
TopModelNum 	-1
Offset      	0.000000 0.000000 0.000000
Direction   	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	0.000000
Damping     	0.000000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	150.000000
UnderRange  	1500.000000
UnderFront	 	450.000000
UnderRear   	144.979996
UnderMax    	0.950000
OverThresh  	281.019989
OverRange   	739.371338
OverMax     	0.520000
OverAccThresh  	668.683472
OverAccRange   	400.000000
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	9830
Aggression     	0
}           	; End AI

;====================
; ;)Camera details
;====================

;)CAMATTACHED {		; Start Camera
;)HoodOffset 	-0.000000 -40.810001 -195.009995 		
;)HoodLook   	0.020000 		
;)RearOffset 	-0.000000 -40.810001 230.009995
;)RearLook   	0.020000
}            		; End Camera


}

22B21BF9